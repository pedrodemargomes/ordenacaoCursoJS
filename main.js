var filaDeTrocas = [];
var vetor = [];
var ordenando = 0;
var NUMERO_BARRAS = 20;

function aleatorizaBarras() {
	if(ordenando == 1) {
		return ;
	}
	var numAleatorio;
	$(".barra").each(function(index){
		numAleatorio = parseInt(500*Math.random());
		$(this).animate({height: numAleatorio.toString()+"px"},150);
		$(this).next().html(numAleatorio);
	});


}

function adicionaBarra() {
	if(ordenando == 1) {
		return ;
	}
	var novaBarra = $("<div></div>").addClass("barra");
	var novaSubBarra = $("<div></div>").addClass("subBarra");
	var novoContainerBarra = $("<div></div>").addClass("containerBarra");

	novaSubBarra.html("50");

	novoContainerBarra.append(novaBarra);
	novoContainerBarra.append(novaSubBarra);

	$("#graf").append(novoContainerBarra);

	novaBarra.bind("click",function() {
		if(ordenando == 1) {
			return ;
		}
		var novoTamanho = prompt("Insira tamanho da barra(entre 0 e 500): ");
		if(novoTamanho == null || novoTamanho < 0 || novoTamanho > 500) {
			alert("Tamanho invalido!");
		} else {
			$(this).animate({height: novoTamanho+"px"},300);
			$(this).next().html(novoTamanho);
		}
	});

	novaSubBarra.bind("click",function() {
		if(ordenando == 1) {
			return ;
		}
		var novoTamanho = prompt("Insira tamanho da barra(entre 0 e 500): ");
		if(novoTamanho == null || novoTamanho < 0 || novoTamanho > 500) {
			alert("Tamanho invalido!");
		} else {
			$(this).prev().animate({height: novoTamanho+"px"},300);
			$(this).html(novoTamanho);
		}
	});

	var numBarras = $(".barra").length;
	$(".barra").each(function(index){
		$(this).parent().css("width", ( 100.0/(numBarras+1) )+"%" );
	});

}

function removeBarra() {
	if(ordenando == 1) {
		return ;
	}
	$(".containerBarra:eq(0)").remove();

	var numBarras = $(".barra").length;
	$(".barra").each(function(index){
		$(this).parent().css("width", ( 100.0/(numBarras+1) )+"%" );
	});
}

function troca(i,j) {
	var aux;
	aux = $(".barra:eq("+i+")").css("height");
	$(".barra:eq("+i+")").css("height", $(".barra:eq("+j+")").css("height") );
	$(".barra:eq("+j+")").css("height", aux);

	aux = $(".barra:eq("+i+")").next().html();
	$(".barra:eq("+i+")").next().html($(".barra:eq("+j+")").next().html() );
	$(".barra:eq("+j+")").next().html(aux);
}

function ordena(){
	if(ordenando == 1) {
		return ;
	}
	ordenando = 1;
	var tempoDeIntervalo = $("#tempoDeIntervalo").val();
	console.log(tempoDeIntervalo);

	$(".subBarra").each(function(index){
		//console.log( index, $(this).html() );
		vetor.push($(this).html());
	});

	var aux;
	for(var i = vetor.length-1; i > 0; i--) {
		for(var j = 0; j < i; j++) {

			if( parseInt(vetor[j]) > parseInt(vetor[j+1]) ) {
				filaDeTrocas.push( {elem1: j, elem2: (j+1)} );

				aux = vetor[j];
				vetor[j] = vetor[j+1];
				vetor[j+1] = aux;
				//console.log(j,j+1);
			}
			
		}
	}

	vetor = [];

	var ultimaTroca = null;
	var numTrocas = 0;
	var timeOrdena = setInterval(function(){
		if(filaDeTrocas.length != 0) {
			numTrocas++;
			if(ultimaTroca != null) {
				$(".barra:eq("+ultimaTroca.elem1+")").css("background-color","red");
				$(".barra:eq("+ultimaTroca.elem2+")").css("background-color","red");
			}
			console.log("Troca: "+numTrocas);
			var elemTroca = filaDeTrocas.shift();
			troca(elemTroca.elem1,elemTroca.elem2);
			ultimaTroca = elemTroca;

			$(".barra:eq("+elemTroca.elem1+")").css("background-color","blue");
			$(".barra:eq("+elemTroca.elem2+")").css("background-color","blue");

		} else {
			console.log("Fim");
			ordenando = 0;
			$(".barra").css("background-color","red");
			clearInterval(timeOrdena);
		}
	},tempoDeIntervalo);
}

for(var i = 0;i< NUMERO_BARRAS;i++) {
	adicionaBarra();
}

$("#ordena").bind("click",ordena);
$("#adiciona").bind("click", adicionaBarra);
$("#remove").bind("click",removeBarra);
$("#aleatorio").bind("click",aleatorizaBarras);
